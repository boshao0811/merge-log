package com.dsp.stock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	
	private static volatile boolean flag = false;
	
	public static void main(String[] args) {
//		A.setA(0);
//		B b = new B();
//		b.test();
//		List<String> list = new ArrayList<String>();
//		list.add("1");
//		list.add("b");
//		args = new String[]{"1","2"};
//		aa(args);
		
		Map<String, Set<String>> map = new ConcurrentHashMap<String, Set<String>>();
		final Set<String> set = new HashSet<String>();
		set.add("b");
		map.put("a", set);
		map.get("a").add("d");
		set.add("c");
		for (String s : map.get("a")) {
			System.out.println(s);
		}
		final Set s = map.get("f") == null? new HashSet<String>():map.get("f");
		s.add("g");
		map.put("f", s);
		for (String s1 : map.get("f")) {
			System.out.println(s1);
		}
		
		String[] a = set.toArray(new String[]{});
		for (String s2 : a) {
			System.out.println(s2);
		}
		
		System.out.println();
		
		
		Thread t = new Thread(){
			public void run(){
				if (set.size()>1) {
					System.out.println("t线程进入判断中");
					synchronized (s) {
						System.out.println("t线程获取线程锁");
						if (set.size()>1) {
							flag = true;
							if (flag) {
								try {
									System.out.println("t线程开始睡眠");
									Thread.sleep(10000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		};
		Thread t1 = new Thread(){
			public void run(){
				if (set.size()>1) {
					System.out.println("t1线程进入判断中");
					synchronized (s) {
						System.out.println("t1线程获取线程锁");
						if (set.size()>1) {
							flag = true;
							if (flag) {
								try {
									System.out.println("t1线程开始睡眠");
									Thread.sleep(10000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
						
						
					}
				}
				
			}
		}; 
		t.start();
		t1.start();
		while (true) {
			if (flag) {
				System.out.println("flag成立执行");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				set.clear();
				flag = false;
			}
		}
		
	}
	public static void aa(String ...a){
		for (String string : a) {
			System.out.println(string);
		}
	}
}

class A {
	private static int a = 1;

	public int getA() {
		return a;
	}

	public static void setA(int a1) {
		a = a1;
	}
	
}

class B extends A {
	public void test(){
		System.out.println(getA());
	}
}
