package com.dsp.stock.conf;

import cn.com.bo.util.PropUtils;

public class Config {
	
	public static String PATH = PropUtils.get("push.path");
	
	public static String PUSH = PATH + "push/{date}/pushlog_{time}_0.log";
	
	public static String SHOW = PATH + "show/{date}/showlog_{time}_0.log";
	
	public static String CLICK = PATH + "click/{date}/clicklog_{time}_0.log";
	
	public static String DSP_LOG_TABLE_ = "dsp_log_";
	
	public static String CREATIVE_TABLE = "creative_table";
	
	public static String LOG_SEPARTOR = ",";
	
	public static String REQ_SEPARTOR = "\001";
	
	public static String REQ_PATH = PropUtils.get("req.path");
	
	public static String EXCHANGE_ = "exchange_";
	
	public static String SHOW_TYPE_ = "show_type_";
	
	public static String SIZE_ = "size_";
	
	public static String MEDIA_ = "media_";
	
	public static String SPACE_ = "space_";
	
	public static String AREA_ = "area_";
	
	public static String CAT_ = "cat_";
	
	public static String HOUR_ = "hour_";
	
	
	
}
