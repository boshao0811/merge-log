package com.dsp.stock.handle;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import cn.com.bo.util.redis.JedisUtil;
import cn.com.bo.util.redis.SerializeUtil;

import com.dsp.stock.conf.Config;
import com.dsp.stock.model.Creative;
import com.dsp.stock.model.Log;
import com.dsp.stock.service.CreativeService;

public class BaseHandle {
	
	private static final SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy_MM_dd_HH");
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	private static JedisUtil jedis = JedisUtil.getInstance();
	
	private CreativeService cs = new CreativeService();
	
	private static Calendar cal = Calendar.getInstance();
	
	protected Log converLog(String[] s) throws NullPointerException {
		if (s.length < 18) {
			return null;
		}
		Log log = new Log();
		log.setTime("" + getRunTime().get(Calendar.HOUR_OF_DAY));
		log.setArea(s[12].replace("\"", ""));
		log.setExchange_id(s[2].replace("\"", ""));
		log.setMedia_id(s[15].replace("\"", ""));
		boolean b = jedis.HASH.hexists(Config.CREATIVE_TABLE, s[4].replace("\"", ""));
		if (!b) {
			if (!cs.addCreative(s[4].replace("\"", ""))) {
				return null;
			}
		}
		Creative c = (Creative)SerializeUtil.unserialize(jedis.HASH.hget(Config.CREATIVE_TABLE.getBytes(), s[4].replace("\"", "").getBytes()));
		log.setSize(c.getSize_id());
		log.setSpace_id(s[5].replace("\"", ""));
		log.setShow_type(c.getShow_type());
		return log;
	}
	
	protected Log converReq(String[] s) {
		if (s.length < 20) {
			return null;
		}
		Log log = new Log();
		log.setTime("" + getRunTime().get(Calendar.HOUR_OF_DAY));
		log.setArea(s[6]);
		log.setCat_id(s[19]);
		log.setExchange_id(s[5]);
		log.setMedia_id(s[8]);
		log.setSize(s[11]);
		log.setSpace_id(s[10]);
		log.setShow_type(s[14]);
		return log;
	}
	
	protected Calendar getRunTime(){
		return cal;
	}
	
	public static void setCal(Calendar cal1){
		cal = cal1;
	}
	
	protected String pushPath(){
		String time = timeFormat.format(getRunTime().getTime());
		String date = dateFormat.format(getRunTime().getTime());
		return Config.PUSH.replace("{date}", date).replace("{time}", time);
	}
	
	protected String showPath() {
		String time = timeFormat.format(getRunTime().getTime());
		String date = dateFormat.format(getRunTime().getTime());
		return Config.SHOW.replace("{date}", date).replace("{time}", time);
	}
	
	protected String clickPath() {
		String time = timeFormat.format(getRunTime().getTime());
		String date = dateFormat.format(getRunTime().getTime());
		return Config.CLICK.replace("{date}", date).replace("{time}", time);
	}
	
	protected File[] reqPath() {
		File reqPath = new File(Config.REQ_PATH);
//		List<String> list = new ArrayList<String>();
//		Calendar start = getRunTime();
//		start.set(Calendar.MINUTE, 0);
//		start.set(Calendar.SECOND, 0);
//		Calendar end = getRunTime();
//		end.set(Calendar.MINUTE, 59);
//		end.set(Calendar.SECOND, 59);
//		for (File file : reqPath.listFiles()) {
////			if (file.lastModified() > start.getTime().getTime() && file.lastModified() < end.getTime().getTime()) {
//			if (file.isFile()) {
//				list.add(file.getPath());
//			}
////			}
//		}
		File[] files = reqPath.listFiles(new java.io.FileFilter() {
			public boolean accept(File f) {
				getRunTime().set(Calendar.MINUTE, 0);
				getRunTime().set(Calendar.SECOND, 0);
				getRunTime().set(Calendar.MILLISECOND, 0);
				long s = getRunTime().getTime().getTime();
				getRunTime().set(Calendar.MINUTE, 59);
				getRunTime().set(Calendar.SECOND, 59);
				getRunTime().set(Calendar.MILLISECOND, 59);
				long e = getRunTime().getTime().getTime();
				long last = f.lastModified();
				if (last > s && last < e) {
					return true;
				}
				return false;
			}
		});
		return files;
	}
	
	public static void main(String[] args) {
		System.out.println(jedis.HASH.hexists("creative_table", "13118_1506080694425"));
	}
}
