package com.dsp.stock.handle;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



import org.apache.log4j.Logger;

import cn.com.bo.util.crypto.MD5;
import cn.com.bo.util.redis.JedisUtil;
import cn.com.bo.util.redis.SerializeUtil;

import com.dsp.stock.conf.Config;
import com.dsp.stock.model.Log;

public class HandlePushLog extends BaseHandle {
	
	private Logger log = Logger.getLogger(HandlePushLog.class);
	
	private JedisUtil jedis = JedisUtil.getInstance();
	
	public void parsePushLog(String table){
		
		log.info("解析投放日志开始!");
		
		List<String> pushList = new ArrayList<String>();
		try {
			pushList = Files.readAllLines(Paths.get(pushPath()), StandardCharsets.UTF_8);
		} catch (IOException e) {
			log.error(pushPath()+",文件解析报错!"+e.getLocalizedMessage());
//			e.printStackTrace();
		}
		for (String p : pushList) {
			Log log = converLog(p.split(Config.LOG_SEPARTOR));
			if (log == null) {
				continue;
			}
			String key = MD5.compute(log.toString());
			if (jedis.HASH.hexists(table, key)) {
				log = (Log)SerializeUtil.unserialize(jedis.HASH.hget(table.getBytes(), key.getBytes()));
				log.setPush(log.getPush()+1);
			} else {
				log.setPush(1);
			}
			jedis.HASH.hset(table, key, SerializeUtil.serialize(log));
			String areaList = Config.AREA_ + log.getArea();
			jedis.SETS.sadd(areaList, key);
			String exchangeList = Config.EXCHANGE_ + log.getExchange_id();
			jedis.SETS.sadd(exchangeList, key);
			String sizeList = Config.SIZE_ + log.getSize();
			jedis.SETS.sadd(sizeList, key);
			String hourList = Config.HOUR_ + log.getTime();
			jedis.SETS.sadd(hourList, key);
			String show_typeList = Config.SHOW_TYPE_ + log.getShow_type();
			jedis.SETS.sadd(show_typeList, key);
			String mediaList = Config.MEDIA_ + log.getMedia_id();
			jedis.SETS.sadd(mediaList, key);
			String spaceList = Config.SPACE_ + log.getSpace_id();
			jedis.SETS.sadd(spaceList, key);
			String catList = Config.CAT_ + log.getCat_id();
			jedis.SETS.sadd(catList, key);
		}
		log.info("解析投放日志结束!");
		
	}
	
	public void parseClickLog(String table){
		log.info("解析点击日志开始!");
		List<String> clickList = new ArrayList<String>();
		try {
			clickList = Files.readAllLines(Paths.get(clickPath()), StandardCharsets.UTF_8);
		} catch (IOException e) {
			log.error(pushPath()+",文件解析报错!"+e.getLocalizedMessage());
			e.printStackTrace();
		}
		for (String c : clickList) {
			Log log = converLog(c.split(Config.LOG_SEPARTOR));
			if (log == null) {
				continue;
			}
			String key = MD5.compute(log.toString());
			if (jedis.HASH.hexists(table, key)) {
				log = (Log)SerializeUtil.unserialize(jedis.HASH.hget(table.getBytes(), key.getBytes()));
				log.setClick(log.getClick()+1);
			} else {
				log.setClick(1);
			}
			jedis.HASH.hset(table, key, SerializeUtil.serialize(log));
			String areaList = Config.AREA_ + log.getArea();
			jedis.SETS.sadd(areaList, key);
			String exchangeList = Config.EXCHANGE_ + log.getExchange_id();
			jedis.SETS.sadd(exchangeList, key);
			String sizeList = Config.SIZE_ + log.getSize();
			jedis.SETS.sadd(sizeList, key);
			String hourList = Config.HOUR_ + log.getTime();
			jedis.SETS.sadd(hourList, key);
			String show_typeList = Config.SHOW_TYPE_ + log.getShow_type();
			jedis.SETS.sadd(show_typeList, key);
			String mediaList = Config.MEDIA_ + log.getMedia_id();
			jedis.SETS.sadd(mediaList, key);
			String spaceList = Config.SPACE_ + log.getSpace_id();
			jedis.SETS.sadd(spaceList, key);
			String catList = Config.CAT_ + log.getCat_id();
			jedis.SETS.sadd(catList, key);
		}
		log.info("解析点击日志结束!");
		
	}
	
	public void parseShowLog(String table){
		log.info("解析展示日志开始!");
		List<String> showList = new ArrayList<String>();
		try {
			showList = Files.readAllLines(Paths.get(showPath()), StandardCharsets.UTF_8);
		} catch (IOException e) {
			log.error(pushPath()+",文件解析报错!"+e.getLocalizedMessage());
			e.printStackTrace();
		}
		for (String s : showList) {
			Log log = converLog(s.split(Config.LOG_SEPARTOR));
			if (log == null) {
				continue;
			}
			String key = MD5.compute(log.toString());
			if (jedis.HASH.hexists(table, key)) {
				log = (Log)SerializeUtil.unserialize(jedis.HASH.hget(table.getBytes(), key.getBytes()));
				log.setShow(log.getShow()+1);
			} else {
				log.setShow(1);
			}
			jedis.HASH.hset(table, key, SerializeUtil.serialize(log));
			String areaList = Config.AREA_ + log.getArea();
			jedis.SETS.sadd(areaList, key);
			String exchangeList = Config.EXCHANGE_ + log.getExchange_id();
			jedis.SETS.sadd(exchangeList, key);
			String sizeList = Config.SIZE_ + log.getSize();
			jedis.SETS.sadd(sizeList, key);
			String hourList = Config.HOUR_ + log.getTime();
			jedis.SETS.sadd(hourList, key);
			String show_typeList = Config.SHOW_TYPE_ + log.getShow_type();
			jedis.SETS.sadd(show_typeList, key);
			String mediaList = Config.MEDIA_ + log.getMedia_id();
			jedis.SETS.sadd(mediaList, key);
			String spaceList = Config.SPACE_ + log.getSpace_id();
			jedis.SETS.sadd(spaceList, key);
			String catList = Config.CAT_ + log.getCat_id();
			jedis.SETS.sadd(catList, key);
		}
		log.info("解析展示日志结束!");
		
	}
	
}
