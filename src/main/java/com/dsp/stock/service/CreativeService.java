package com.dsp.stock.service;

import java.util.List;

import cn.com.bo.util.mysql.MysqlUtil;
import cn.com.bo.util.redis.JedisUtil;
import cn.com.bo.util.redis.SerializeUtil;

import com.dsp.stock.conf.Config;
import com.dsp.stock.model.Creative;

public class CreativeService {
	
	private JedisUtil jedis = JedisUtil.getInstance();
	
	public static List<Creative> getList(){
		List<Creative> list = MysqlUtil.find(new Creative(), "select cid,show_type,size_id from creative_info");
		return list;
	}
	
	public static Creative getCreativeByCid(String cid){
		Creative c = MysqlUtil.findOne(new Creative(), "select cid,show_type,size_id from creative_info where cid = "+ cid);
		return c;
	}
	
	public void initCreative(){
		jedis.HASH.hdel(Config.CREATIVE_TABLE);
		List<Creative> list = getList();
		for (Creative creative : list) {
			jedis.HASH.hset(Config.CREATIVE_TABLE, creative.getCid(), SerializeUtil.serialize(creative));
		}
	}
	
	public boolean addCreative(String cid) {
		Creative c = getCreativeByCid(cid);
		if (c == null) {
			return false;
		}
		return jedis.HASH.hset(Config.CREATIVE_TABLE, c.getCid(), SerializeUtil.serialize(c)) == 1;
	}
	
	public static void main(String[] args) {
		List<Creative> list = getList();
		System.out.println(list.size());
	}
	
}	
