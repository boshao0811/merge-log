package com.dsp.stock.model;

import java.io.Serializable;

public class Log implements Serializable {
	
	private static final long serialVersionUID = 65029074044362433L;
	
	private String exchange_id;
	
	private String size;
	
	private String show_type;
	
	private String media_id;
	
	private String space_id;
	
	private String area;
	
	private String time;
	
	private String cat_id;
	
	private long pv;
	
	private long push;
	
	private long show;
	
	private long click;

	public String getExchange_id() {
		return exchange_id;
	}

	public void setExchange_id(String exchange_id) {
		this.exchange_id = exchange_id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getShow_type() {
		return show_type;
	}

	public void setShow_type(String show_type) {
		this.show_type = show_type;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getSpace_id() {
		return space_id;
	}

	public void setSpace_id(String space_id) {
		this.space_id = space_id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCat_id() {
		return cat_id;
	}

	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public long getPv() {
		return pv;
	}

	public void setPv(long pv) {
		this.pv = pv;
	}

	public long getPush() {
		return push;
	}

	public void setPush(long push) {
		this.push = push;
	}

	public long getShow() {
		return show;
	}

	public void setShow(long show) {
		this.show = show;
	}

	public long getClick() {
		return click;
	}

	public void setClick(long click) {
		this.click = click;
	}

	
	public String toString() {
		return exchange_id + "," + size
				+ "," + show_type + "," + media_id
				+ "," + space_id + "," + area + ","
				+ time + "," + cat_id;
	}
	
}
