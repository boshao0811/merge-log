package com.dsp.stock.model;

import java.io.Serializable;

public class Creative implements Serializable {
	
	private static final long serialVersionUID = 6502907404436243638L;
	
	private String cid;
	
	private String show_type;
	
	private String size_id;
	
	private String sw;
	
	private String sh;

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getShow_type() {
		return show_type;
	}

	public void setShow_type(String show_type) {
		this.show_type = show_type;
	}

	public String getSize_id() {
		return size_id;
	}

	public void setSize_id(String size_id) {
		this.size_id = size_id;
	}

	public String getSw() {
		return sw;
	}

	public void setSw(String sw) {
		this.sw = sw;
	}

	public String getSh() {
		return sh;
	}

	public void setSh(String sh) {
		this.sh = sh;
	}
	
	
	
}
