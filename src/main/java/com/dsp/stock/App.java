package com.dsp.stock;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.xml.sax.HandlerBase;

import com.dsp.stock.conf.Config;
import com.dsp.stock.handle.BaseHandle;
import com.dsp.stock.handle.HandlePushLog;
import com.dsp.stock.handle.HandleReqLog;
import com.dsp.stock.model.Log;
import com.dsp.stock.service.CreativeService;
import com.sun.mail.util.PropUtil;

import cn.com.bo.util.PropUtils;
import cn.com.bo.util.crypto.MD5;
import cn.com.bo.util.mysql.MysqlUtil;
import cn.com.bo.util.redis.JedisUtil;

public class App 
{
	
	private static JedisUtil jedis = JedisUtil.getInstance();
	
	private static CreativeService creativeService = new CreativeService();
	
    public static void main( String[] args )
    {
    	if (args != null && args.length > 0) {
    		PropUtils.init(args[0]);
        	MysqlUtil.reloadSource();
		}
    	
    	long length = jedis.getHash().hlen(Config.CREATIVE_TABLE);
    	if (length == 0) {
    		creativeService.initCreative();
		}
    	
    	Calendar cal = Calendar.getInstance();
		BaseHandle.setCal(cal);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(sdf.format(cal.getTime()));
    	String table = Config.DSP_LOG_TABLE_ + cal.get(Calendar.HOUR_OF_DAY);
    	jedis.HASH.hdel(table);
    	
    	
    	HandleReqLog hrl = new HandleReqLog();
    	hrl.parseReqLog(table);
    	
    	HandlePushLog hpl = new HandlePushLog();
    	hpl.parsePushLog(table);
    	hpl.parseShowLog(table);
    	hpl.parseClickLog(table);
    	
    }
}
